ICD-11 is licensed under the Creative Commons Attribution-NoDerivs 3.0 IGO license (CC BY-ND 3.0 IGO).

More information on the license for the ICD-11 and related software can be found at our ICD-11 Terms of Use and License Agreement document:
https://icd.who.int/en/docs/icd11-license.pdf