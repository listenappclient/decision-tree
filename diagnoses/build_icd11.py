import sys
import time
import json
import requests
from bs4 import BeautifulSoup


# this code parse and build the ICD-11 MMS JSON tree

out_dir = sys.argv[1] # the output directory as argument

icd_11_chapters = [{"foundation_id":1435254666, "chapter":"01", "description":"Certain infectious or parasitic diseases"},
					{"foundation_id":1630407678, "chapter":"02", "description":"Neoplasms"},
					{"foundation_id":1766440644, "chapter":"03", "description":"Diseases of the blood or blood-forming organs"},
					{"foundation_id":1954798891, "chapter":"04", "description":"Diseases of the immune system"},
					{"foundation_id":21500692, "chapter":"05", "description":"Endocrine, nutritional or metabolic diseases"},
					{"foundation_id":334423054, "chapter":"06", "description":"Mental, behavioural or neurodevelopmental disorders"},
					{"foundation_id":274880002, "chapter":"07", "description":"Sleep-wake disorders"},
					{"foundation_id":1296093776, "chapter":"08", "description":"Diseases of the nervous system"},
					{"foundation_id":868865918, "chapter":"09", "description":"Diseases of the visual system"},
					{"foundation_id":1218729044, "chapter":"10", "description":"Diseases of the ear or mastoid process"},
					{"foundation_id":426429380, "chapter":"11", "description":"Diseases of the circulatory system"},
					{"foundation_id":197934298, "chapter":"12", "description":"Diseases of the respiratory system"},
					{"foundation_id":1256772020, "chapter":"13", "description":"Diseases of the digestive system"},
					{"foundation_id":1639304259, "chapter":"14", "description":"Diseases of the skin"},
					{"foundation_id":1473673350, "chapter":"15", "description":"Diseases of the musculoskeletal system or connective tissue"},
					{"foundation_id":30659757, "chapter":"16", "description":"Diseases of the genitourinary system"},
					{"foundation_id":577470983, "chapter":"17", "description":"Conditions related to sexual health"},
					{"foundation_id":714000734, "chapter":"18", "description":"Pregnancy, childbirth or the puerperium"},
					{"foundation_id":1306203631, "chapter":"19", "description":"Certain conditions originating in the perinatal period"},
					{"foundation_id":223744320, "chapter":"20", "description":"Developmental anomalies"},
					{"foundation_id":1843895818, "chapter":"21", "description":"Symptoms, signs or clinical findings, not elsewhere classified"},
					{"foundation_id":435227771, "chapter":"22", "description":"Injury, poisoning or certain other consequences of external causes"},
					{"foundation_id":850137482, "chapter":"23", "description":"External causes of morbidity or mortality"},
					{"foundation_id":1249056269, "chapter":"24", "description":"Factors influencing health status or contact with health services"},
					{"foundation_id":1596590595, "chapter":"25", "description":"chapters for special purposes"},
					{"foundation_id":718687701, "chapter":"26", "description":"Supplementary Chapter Traditional Medicine Conditions - Module I"},
					{"foundation_id":231358748, "chapter":"V", "description":"Supplementary section for functioning assessment"},
					{"foundation_id":979408586, "chapter":"X", "description":"Extension Codes"}]

def get_children(id, adopted_children='true'):

	'''
		Gets foundation_id children info.

			:param foundation_id (int): the icd-11 foundation id as a root node
			:param adopted_children (str): indicate wethere or not to include icd-11 adopted children
	'''

	url = f'https://icd.who.int/browse11/l-m/en/JsonGetChildrenConcepts?ConceptId=http://id.who.int/icd/entity/{id}&useHtml=true&showAdoptedChildren={adopted_children}'
	while True:
		try:
			page = requests.get(url=url, timeout=30, params={"User-Agent":'Mozilla/5.0'}).text
			break
		except requests.ConnectionError as e:
			print("\nOOPS!! Connection error. Make sure you are connected to Internet. Technical details given below.\n")
			print(str(e)) 
			time.sleep(5)
			continue
		except requests.Timeout as e:
			print("\nOOPS!! Timeout error. Technical details given below.\n")
			print(str(e))
			continue
		except requests.RequestException as e:
			print("\nOOPS!! General error. Technical details given below.\n")
			print(str(e))
			continue

	content = json.loads(page)
	return content

def build_tree(tree, ancestor=None):

	'''
		Expands the tree from a single node.

		:param tree (dict): The node to build the tree from. The key "foundation_id" is required
		:param ancestor (int): the ancestor node of the node.
	'''
	node = tree['foundation_id']
	if tree['foundation_id'] in ['other','unspecified']:
		children = get_children('/'.join([str(ancestor),'mms',node]))
	else:
		children = get_children(tree['foundation_id'])

	if len(children) == 0: # recursion stop condition
		return tree
	
	lst = []
	for child in children:
		child_id = child['ID'].split('/')[-1]
		page = BeautifulSoup(child['html'], 'html.parser')
		child_code = page.span.text

		if child_code in ['','(TM1)']:
			if child_code == '':
				child_desc = page.text.split('\r\n')[1].split('\n')[0]
			elif child_code == '(TM1)':
				child_desc = page.text.split('\r\n')[5].split('\n')[0]
			child_code = None
		else:
			child_desc = page.text.split('\r\n')[1].split('\n')[0]

		if (child_id == "unspecified") & (not child_desc.endswith('unspecified')):
			child_desc = child_desc + ', unspecified'

		lst.append({'foundation_id':child_id, 'code':child_code, 'description':child_desc})
	branches = [build_tree(branch, ancestor=node) for branch in lst]
	tree['children'] = branches
	return tree


# build the ICD-11 MMS tree
icd_11_catalog = {"ICD-11 for Mortality and Morbidity Statistics (ICD-11 MMS)": {f"Chapter {chapter['chapter']}": build_tree(chapter) for chapter in icd_11_chapters}}

# writing the ICD-11 tree to JSON file
with open(out_dir + '/icd11_tree.json', 'w') as json_file:
	json.dump(icd_11_catalog, json_file)