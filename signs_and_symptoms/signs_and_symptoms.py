from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import lit

conf = SparkConf().setAppName("test")
conf.set("spark.driver.memory", "28g")
conf.set("spark.local.dir", "/home/gilf/spark_shuffle")
sc = SparkContext(conf=conf, master="local[*]")
spark = SparkSession.builder.getOrCreate()

cncpt = spark.read.options(header='true', inferSchema='false', sep='\t').csv("/mnt/c/users/gilfr/documents/research/OMOP_CDM/vocabulary_v5_05_21/concept.csv")
cncpt = cncpt.where(cncpt.invalid_reason.isNull()).where(cncpt.domain_id.isin("Observation","Condition"))
cncpt = cncpt.drop("domain_id","concept_class_id","standard_concept","valid_start_date","valid_end_date","invalid_reason")

relation = spark.read.options(header='true', inferSchema='false', sep='\t').csv("/mnt/c/users/gilfr/documents/research/OMOP_CDM/vocabulary_v5_05_21/concept_relationship.csv")
relation = relation.where(relation.invalid_reason.isNull()).where(relation.relationship_id == "Subsumes").withColumnRenamed("concept_id_1","concept_id")
relation = relation.select("concept_id","concept_id_2")

signs = relation.where(relation.concept_id.isin("4156363","4303401"))

signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.withColumn("Treatment", lit("Signs and Symptoms"))
signs = signs.select("Treatment", signs.vocabulary_id.alias("Catalog"), signs.concept_id_2.alias("concept_id"))
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", signs.concept_name.alias("Level 1"), signs.concept_code.alias("Level 1 Code"), signs.vocabulary_id.alias("Level 1 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel2 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel2 = naLevel2.withColumnRenamed("concept_id_2","Level 2").withColumn("Level 2 Code", lit(None)).withColumn("Level 2 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", signs.concept_name.alias("Level 2"), signs.concept_code.alias("Level 2 Code"), signs.vocabulary_id.alias("Level 2 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel3 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel3 = naLevel3.withColumnRenamed("concept_id_2","Level 3").withColumn("Level 3 Code", lit(None)).withColumn("Level 3 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", signs.concept_name.alias("Level 3"), signs.concept_code.alias("Level 3 Code"), signs.vocabulary_id.alias("Level 3 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel4 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel4 = naLevel4.withColumnRenamed("concept_id_2","Level 4").withColumn("Level 4 Code", lit(None)).withColumn("Level 4 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", signs.concept_name.alias("Level 4"), signs.concept_code.alias("Level 4 Code"), signs.vocabulary_id.alias("Level 4 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel5 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel5 = naLevel5.withColumnRenamed("concept_id_2","Level 5").withColumn("Level 5 Code", lit(None)).withColumn("Level 5 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", "Level 4", "Level 4 Code", "Level 4 Catalog", signs.concept_name.alias("Level 5"), signs.concept_code.alias("Level 5 Code"), signs.vocabulary_id.alias("Level 5 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel6 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel6 = naLevel6.withColumnRenamed("concept_id_2","Level 6").withColumn("Level 6 Code", lit(None)).withColumn("Level 6 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", "Level 4", "Level 4 Code", "Level 4 Catalog", "Level 5", "Level 5 Code", "Level 5 Catalog", signs.concept_name.alias("Level 6"), signs.concept_code.alias("Level 6 Code"), signs.vocabulary_id.alias("Level 6 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel7 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel7 = naLevel7.withColumnRenamed("concept_id_2","Level 7").withColumn("Level 7 Code", lit(None)).withColumn("Level 7 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", "Level 4", "Level 4 Code", "Level 4 Catalog", "Level 5", "Level 5 Code", "Level 5 Catalog", "Level 6", "Level 6 Code", "Level 6 Catalog", signs.concept_name.alias("Level 7"), signs.concept_code.alias("Level 7 Code"), signs.vocabulary_id.alias("Level 7 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel8 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel8 = naLevel8.withColumnRenamed("concept_id_2","Level 8").withColumn("Level 8 Code", lit(None)).withColumn("Level 8 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", "Level 4", "Level 4 Code", "Level 4 Catalog", "Level 5", "Level 5 Code", "Level 5 Catalog", "Level 6", "Level 6 Code", "Level 6 Catalog", "Level 7", "Level 7 Code", "Level 7 Catalog", signs.concept_name.alias("Level 8"), signs.concept_code.alias("Level 8 Code"), signs.vocabulary_id.alias("Level 8 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel9 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel9 = naLevel9.withColumnRenamed("concept_id_2","Level 9").withColumn("Level 9 Code", lit(None)).withColumn("Level 9 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", "Level 4", "Level 4 Code", "Level 4 Catalog", "Level 5", "Level 5 Code", "Level 5 Catalog", "Level 6", "Level 6 Code", "Level 6 Catalog", "Level 7", "Level 7 Code", "Level 7 Catalog", "Level 8", "Level 8 Code", "Level 8 Catalog", signs.concept_name.alias("Level 9"), signs.concept_code.alias("Level 9 Code"), signs.vocabulary_id.alias("Level 9 Catalog"), "concept_id")

signs = signs.join(relation, on=["concept_id"], how="left")
naLevel10 = signs.where(signs.concept_id_2.isNull()).drop("concept_id")
naLevel10 = naLevel10.withColumnRenamed("concept_id_2","Level 10").withColumn("Level 10 Code", lit(None)).withColumn("Level 10 Catalog", lit(None))
signs = signs.na.drop(subset=["concept_id_2"]).drop("concept_id")
signs = signs.withColumnRenamed("concept_id_2", "concept_id")
signs = signs.join(cncpt, on=["concept_id"], how="inner")
signs = signs.select("Treatment", "Catalog", "Level 1", "Level 1 Code", "Level 1 Catalog", "Level 2", "Level 2 Code", "Level 2 Catalog", "Level 3", "Level 3 Code", "Level 3 Catalog", "Level 4", "Level 4 Code", "Level 4 Catalog", "Level 5", "Level 5 Code", "Level 5 Catalog", "Level 6", "Level 6 Code", "Level 6 Catalog", "Level 7", "Level 7 Code", "Level 7 Catalog", "Level 8", "Level 8 Code", "Level 8 Catalog", "Level 9", "Level 9 Code", "Level 9 Catalog", signs.concept_name.alias("Level 10"), signs.concept_code.alias("Level 10 Code"), signs.vocabulary_id.alias("Level 10 Catalog"), "concept_id")
signs = signs.drop("concept_id")

for l in range(3,11):
	naLevel2 = naLevel2.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))

for l in range(4,11):
	naLevel3 = naLevel3.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))
	
for l in range(5,11):
	naLevel4 = naLevel4.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))
	
for l in range(6,11):
	naLevel5 = naLevel5.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))
	
for l in range(7,11):
	naLevel6 = naLevel6.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))
	
for l in range(8,11):
	naLevel7 = naLevel7.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))

for l in range(9,11):
	naLevel8 = naLevel8.withColumn("Level "+str(l), lit(None)).withColumn("Level {} Code".format(l), lit(None)).withColumn("Level {} Catalog".format(l), lit(None))
	
naLevel9 = naLevel9.withColumn("Level 10", lit(None)).withColumn("Level 10 Code", lit(None)).withColumn("Level 10 Catalog", lit(None))

signs = naLevel2.union(naLevel3).union(naLevel4).union(naLevel5).union(naLevel6).union(naLevel7).union(naLevel8).union(naLevel9).union(naLevel10).union(signs)
signs = signs.orderBy(["Level "+str(l) for l in range(1,11)], ascending=True)
signs.coalesce(1).write.options(header='true', encoding='utf-8', sep=',', emptyValue="").csv("/mnt/c/users/gilfr/documents/research/signs_and_symptoms")
