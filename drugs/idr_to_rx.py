import sys
import json
import requests
from fuzzywuzzy import fuzz
import pandas as pd
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, LongType
from pyspark.sql.functions import lower

# this script tries to map Israeli drugs to RXNORM international prescribable names

def athena_web_scraper(input_text, domain, vocabs, category, page_size=100):
	'''
	Web scraping from Athena search engine.
	The result content returned as a Spark DataFrame.

	:param input_text (str): the string to find a match for. E.g.: "Diabetes mellitus" (required).
	:param domain (str): the desired domain of the input text. E.g.: "Condition","Procedure","Drug" (required).
	:param vocabs (list of strings): the desired vocabularies of the input text. E.g.: "ATC","NDA","RxNorm" (required).
	:param category (string): the desired class of the input text (required). Available value are:
				- "BN" for Brand Name.
				- "SBDF" for Semantic Branded Drug Form Group.
				- "SBD" for Semantic branded drug Group.
				- "SCDF" for Semantic Clinical Drug Form.
				- "SCD" for Semantic Clinical Drug
	:param page_size (int): number of search suggestions (optional). Default is 100.
	'''

	if category == "BN":
		category = "Brand+Name"
	elif category == "SBDF":
		category = "Branded+Drug+Form"
	elif category == "SBD":
		category = "Branded+Drug"
	elif category == "SCDF":
		category = "Clinical+Drug+Form"
	elif category == "SCD":
		category = "Clinical+Drug"

	vocabs = [v.replace(' ', '+') for v in vocabs]
	search_text = input_text.replace(' ', '%20')
	search_url = f"https://athena.ohdsi.org/api/v1/concepts?domain={domain}" + ''.join([f"&vocabulary={v}" for v in vocabs]) + f"&conceptClass={category}&invalidReason=Valid&pageSize={page_size}&query={search_text}"
	html_page = requests.get(url=search_url, params={'User-Agent': 'Mozilla/5.0'}).text
	content = json.loads(html_page)['content']

	return content

def generateDataFrameKeys(spark, df, offset=1, colName="id"):
	'''
		Enumerates dataframe rows is native order, like rdd.ZipWithIndex(), but on a dataframe
		and preserves a schema

		:param df: source dataframe
		:param offset: adjustment to zipWithIndex()'s index
		:param colName: name of the index column
	'''

	zipped_rdd = df.rdd.zipWithIndex() # convert to RDD and generate new keys

	new_schema = StructType(
		[StructField(colName,LongType(),True)]        # new added field in front
		+ df.schema.fields)                            # previous schema

	new_rdd = zipped_rdd.map(lambda args: ([args[1] + offset] + list(args[0])))      # use this for python 3+, tuple gets passed as single argument so using args and [] notation to read elements within args
	new_df = spark.createDataFrame(new_rdd, new_schema)

	return new_df


conf = SparkConf().setAppName("Athena Web Scraper")
conf.set("spark.local.dir", "/home/gil/spark_shuffle")
conf.set("spark.driver.memory", "10g")
sc = SparkContext(conf=conf, master="local[*]")
spark = SparkSession.builder.getOrCreate()

# fetch IDR brands
with open('/home/gil/files/idr_database.json') as f:
	drugs = json.load(f)
	f.close()

drug_ids = [int(x) for x in list(drugs.keys())]
brands = []
for drug_id in drugs.keys():
	brand_name = drugs[drug_id]['brandName']['en']
	brands.append(brand_name)

schema = StructType([StructField('id', LongType(), False),
			StructField('code', StringType(), False),
			StructField('name', StringType(), False),
			StructField('className', StringType(), False),
			StructField('standardConcept', StringType(), False),
			StructField('invalidReason', StringType(), False),
			StructField('domain', StringType(), False),
			StructField('vocabulary', StringType(), False)])

for i in range(len(drug_ids)):
	drug_id = drug_ids[i]
	if str(drug_id) == "10039":
		break
	brand = brands[i]
	out_path = "/mnt/c/Users/gilor/Documents/Research/Medications/temp/" + f"{drug_id}__{brand}.csv".replace('/','-')
	content = athena_web_scraper(brand, "Drug", ["RxNorm", "RxNorm Extension"], "BN")
	spark_df = spark.createDataFrame(data=content, schema=schema)
	spark_df = generateDataFrameKeys(spark, spark_df, colName="key")
	spark_df = spark_df.withColumn("lower_case_name", lower(spark_df["name"]))
	spark_df = spark_df.drop_duplicates(subset=["lower_case_name"])
	spark_df = spark_df.orderBy("key", ascending=True)
	spark_df = spark_df.drop("key","className","standardConcept","invalidReason","domain","lower_case_name")
	spark_df = spark_df.select(spark_df.id.alias("concept_id"),spark_df.code.alias("RxCUI"),spark_df.name.alias("brand name"),spark_df.vocabulary.alias("catalog"))
	spark_df = spark_df.limit(20)
	matched_drugs = [d['brand name'] for d in spark_df.select("brand name","RxCUI").collect()]
	for d1 in matched_drugs:
		for d2 in matched_drugs:
			if (d1 != d2) & (fuzz.ratio(d1, d2) / 100 >= 0.98):
				matched_drugs.remove(d2)
	spark_df = spark_df.where(spark_df['brand name'].isin(matched_drugs))
	lookup_tbl = spark_df.toPandas()
	if len(lookup_tbl) > 0:
		lookup_tbl.to_csv(out_path, index=False)
