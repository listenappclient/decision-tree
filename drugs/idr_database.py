from numpy.lib.shape_base import split
import requests
import re
import json
from bs4 import BeautifulSoup
import pandas as pd
from requests.api import head

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)

# this script parse, process and export the Israeli Drug Database belongs to the Israeli Pharmacy Organization

# payload = {"dragRegNum": "128 74 25337 08"}

# headers = {"Accept": "application/json",
# "Accept-Encoding": "br",
# "Accept-Language": "en-US",
# "Connection": "keep-alive",
# "Content-Length": "32",
# "Content-Type": "application/json",
# "Host": "esb.gov.il",
# "Origin": "https://data.health.gov.il",
# "Referer": "https://data.health.gov.il/",
# "sec-ch-ua": '"Google Chrome";v="91"',
# "sec-ch-ua-mobile": "?0",
# "Sec-Fetch-Dest": "empty",
# "Sec-Fetch-Mode": "cors",
# "Sec-Fetch-Site": "cross-site",
# "User-Agent": "Mozilla/5.0"}

# r = requests.post(url="https://esb.gov.il/GovServiceList/IDRServer/GetSpecificDrug", data=payload, verify=False)


## Israeli drug organization

# drug_ids = []
# for atc_id in range(1,1000):
# 	r = requests.get(f'https://www.drug.co.il/result.asp?atc={atc_id}', {"User-Agent":"Mozilla/5.0"})
# 	page = bytes(r.text, encoding='iso-8859-1').decode('iso-8859-8').split('<!-- *********************************************  //links table  ********************************-->')[1]
# 	content =  BeautifulSoup(page, 'html.parser')
# 	vals = []
# 	for elem in content.find_all():
# 		vals += list(elem.attrs.values())
# 	ids = set([v.split('drugID=')[1] for v in vals if 'drugID' in v])
# 	drug_ids += ids
# drug_ids = sorted(list(set(drug_ids)))
# drug_ids = pd.DataFrame(drug_ids, columns=['id']).astype('int64')
# drug_ids.to_csv('/home/gil/scripts/search_engine/medications/idr_drug_ids.csv', index=False, encoding='utf-8')

def nullify(x):
	if x:
		if len(x) == 0:
			return None
		else:
			return x
	else:
		return None
		
drug_ids = pd.read_csv('/home/gil/scripts/search_engine/medications/idr_drug_ids.csv', encoding='utf-8')['id'].tolist()
records = {}
for id in drug_ids:

	record = {}
	url = f'https://www.drug.co.il/bygeneric.asp?drugID={id}'

	while True:
		try:
			r = requests.get(url=url, timeout=30, params={"User-Agent":'Mozilla/5.0'})
			break
		except requests.ConnectionError as e:
			print("\nOOPS!! Connection error. Make sure you are connected to Internet. Technical details given below.\n")
			print(str(e))
			continue
		except requests.Timeout as e:
			print("\nOOPS!! Timeout error. Technical details given below.\n")
			print(str(e))
			continue
		except requests.RequestException as e:
			print("\nOOPS!! General error. Technical details given below.\n")
			print(str(e))
			continue

	page = bytes(r.text, encoding='iso-8859-1').decode('iso-8859-8')
	page_content = BeautifulSoup(page, 'html.parser')

	links_page = page.split('<!-- *********************************************  links table  ********************************-->')[1]
	links_page = links_page.split('<!-- *********************************************  //links table  ********************************-->')[0]
	links_page = links_page.split('<!-- ********** Update  ACTIVE details -->')
	links_page = [p for p in links_page if 'מנגנון פעילות:' in p]
	links_content = [BeautifulSoup(p, 'html.parser') for p in links_page]

	results_page = page.split('<!-- *********************************************  results  ********************************-->')[1]
	results_page = results_page.split('<!-- *********************************************  results header  ********************************-->')[0]
	results_content = BeautifulSoup(results_page, 'html.parser')

	header_page = page.split('<!-- *********************************************  results header  ********************************-->')[1]
	header_page = header_page.split('<!-- *********************************************  //results header  ********************************-->')[0]
	header_content = BeautifulSoup(header_page, 'html.parser')

	ingr_page = page.split("<!-- ********************************************* ingredients ********************************-->")[1]
	ingr_page = ingr_page.split("<!-- ********************************************* //ingredients ********************************-->")[0]
	ingr_content = BeautifulSoup(ingr_page, 'html.parser')

	atc_page = page.split('<!-- ********************************************* ATC ********************************-->')[1]
	atc_page = atc_page.split('<!-- ********************************************* //ATC ********************************-->')[0]
	atc_content = BeautifulSoup(atc_page, 'html.parser')

	pic_symbol_page = page.split('<!-- *********************************************  //picture & symbols table  ********************************-->')[1]
	pic_symbol_page = pic_symbol_page.split('<!-- ********************************************* ingredients ********************************-->')[0]
	pic_symbol_content = BeautifulSoup(pic_symbol_page, 'html.parser')

	packages_page = page.split('<!-- ********************************************* price ********************************-->')[1]
	packages_page = packages_page.split('<!-- ********************************************* //price ********************************-->')[0]
	packages_content = BeautifulSoup(packages_page, 'html.parser')


	generic_name = page_content.title.text.split('drug.co.il - ')[1]
	brand_name_en, brand_name_he = [nullify(item.text.replace(u'\xa0',u' ')) for item in header_content.find_all('td')]

	if len(links_content) == 0:
		mechanisms = None
	else:
		mechanisms = {}
		for item in links_content:
			if item.b:
				if item.b.text:
					active_ingr = item.b.text
					mechanism = re.sub('\n|\r|\t', '', item.find_all('tr')[-1].text.split('מנגנון פעילות:')[1])
					mechanisms.update({active_ingr: mechanism})

	for item in pic_symbol_content.find_all('tr'):
		if item.b:
			if item.b.text == "בסל הבריאות?:":
				if item.td.text == "לא":
					sal_index = 0
				else:
					sal_index = 1
			elif item.b.text == "התוויה מאושרת:":
				indication = nullify(item.td.text)
			elif item.b.text == "דרך מתן:":
				drug_form = nullify(item.td.text)
			elif item.b.text == "בעל רישום:":
				owner = nullify(item.td.text)
			elif item.b.text == "מספר רישום:":
				registration_id = nullify(item.td.text)

	atc_desc = list(set([nullify(elem.text) for elem in atc_content.find_all("span")]))
	atc_desc = sorted([code for code in atc_desc if code])

	ingredients = None
	if 'כמות' in page_content.text:
		ingredients = []
		for row in ingr_content.find_all('tr')[0].find_all('tr')[2:]:
			cols = row.find_all('td')
			if len(cols) ==  2:
				ingr = nullify(cols[0].text.replace('   ',' ').replace('  ',' '))
				strength = nullify(cols[1].text.replace('   ',' ').replace('  ',' '))
				ingredients.append({"ingredient": ingr, "strength": strength})

	packages = None
	if 'קוד ירפא' in page_content.text:
		packages = []
		for row in packages_content.find_all('tr')[0].find_all('tr'):
			cols = row.find_all('td')
			if len(cols) < 4:
				continue
			try:
				price = float(cols[0].text.replace(',',''))
			except ValueError:
				price = None
			try:
				quantity = int(cols[1].text)
			except ValueError:
				quantity = None
			pkg_desc = nullify(re.sub('\n|\r|\t', '', cols[2].text).replace('   ',' ').replace('  ',' '))
			if pkg_desc == "תאור אריזה":
				continue
			try:
				yrpa_id = int(cols[3].text)
			except ValueError:
				yrpa_id = None
			
			if not all([x is None for x in [price,quantity,pkg_desc,yrpa_id]]):	
				packages.append({"PRICE": price, "QTY": quantity, "PKG_DESCRIPTION": pkg_desc, "YRPA_ID": yrpa_id})

	application_date = None
	restrictions = None
	definitions = None
	pkg_or_strength = None
	common_drugs = None
	if sal_index == 1:
		sal_url = f'https://drug.co.il/get_SAL.asp?DrugID={id}'
		while True:
			try:
				sal = requests.get(url=sal_url, timeout=30, params={"User-Agent":'Mozilla/5.0'})
				break
			except requests.ConnectionError as e:
				print("\nOOPS!! Connection error. Make sure you are connected to Internet. Technical details given below.\n")
				print(str(e))
				continue
			except requests.Timeout as e:
				print("\nOOPS!! Timeout error. Technical details given below.\n")
				print(str(e))
				continue
			except requests.RequestException as e:
				print("\nOOPS!! General error. Technical details given below.\n")
				print(str(e))
				continue

		sal = bytes(sal.text, encoding='iso-8859-1').decode('iso-8859-8')
		sal_page = BeautifulSoup(sal, 'html.parser')
		for item in sal_page.find_all('tr'):
			if item.b:
				if item.b.text == "תאריך החלה:":
					application_date = nullify(item.td.text)
				elif item.b.text == "הגבלות:":
					restrictions = nullify(item.td.text)
				elif item.b.text == "הגדרות:":
					definitions = nullify(item.td.text)
				elif item.b.text == "אריזה/חוזק:":
					pkg_or_strength = nullify(item.td.text)
				elif item.b.text == "תרופות בקבוצה:":
					common_drugs = nullify(item.td.text)

	related_brands = []
	for item in results_content.find_all('tr'):
		if item.a:
			related_id = int(item.a['&drugid'])
			related_brand = item.find_all('td')[-1].text
			related_brands.append({"drugID": related_id, "brandName": related_brand})


	record.update({"drugID": id,
				"brandName": {"en": brand_name_en, "he": brand_name_he},
				"genericName": generic_name,
				"approvedIndication": indication,
				"drugForm": drug_form,
				"ATC": atc_desc,
				"registrationOwner": owner,
				"registrationID": registration_id,
				"activeIngredients": ingredients,
			 	"drugBasket": {"included": sal_index,
				 			"applicationDate": application_date,
							"restrictions":restrictions, 
							"definitions": definitions,
							"pkgOrStrength": pkg_or_strength,
							"commonDrugs": common_drugs},
				"packages": nullify(packages),
				"mechanismOfAction": mechanisms,
				"relatedBrands": nullify(related_brands)})

	records.update({id: record})

with open('/home/gil/files/idr_database.json', mode='w') as f:
	json.dump(records, fp=f, indent=10)
	f.close()
