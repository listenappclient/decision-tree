import os
import sys
import argparse
import json
import requests
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, LongType
from pyspark.sql.functions import lower
from fuzzywuzzy import fuzz

# this script simulates a search engine performs a lookup in diagnoses and medical procedures

global_dir = os.environ.get("GLOBAL_DIR")

def writeDataFrameToFS(df, filePath, withHeader=True, encoding="utf-8", format="csv", isZipped=False):
	'''
		Writes spark dataframe to disk.

		:param df: the spark DataFrame to write to disk
		:param filePath: the desired directory path to save the DataFrame into.
		:param withHeader: indicator for keeping the DataFrame header
		:param encoding: desired encoding when writing to disk
		:param format: desired format of the written data (E.g. csv, json)
		:param isZipped: indicator wethere to compress the output
	'''

	sparkWriter = df.write.options(header=withHeader, encoding=encoding, emptyValue="")
	if isZipped:
		sparkWriter = sparkWriter.option("codec", "org.apache.hadoop.io.compress.BZip2Codec")
	sparkWriter.format(format).mode("overwrite").save(filePath)

def generateDataFrameKeys(spark, df, offset=1, colName="id"):
	'''
		Enumerates dataframe rows is native order, like rdd.ZipWithIndex(), but on a dataframe
		and preserves a schema

		:param df: source dataframe
		:param offset: adjustment to zipWithIndex()'s index
		:param colName: name of the index column
	'''

	zipped_rdd = df.rdd.zipWithIndex() # convert to RDD and generate new keys

	new_schema = StructType(
		[StructField(colName,LongType(),True)]        # new added field in front
		+ df.schema.fields)                            # previous schema

	new_rdd = zipped_rdd.map(lambda args: ([args[1] + offset] + list(args[0])))      # use this for python 3+, tuple gets passed as single argument so using args and [] notation to read elements within args
	new_df = spark.createDataFrame(new_rdd, new_schema)
	
	return new_df

def athena_web_scraper(input_text, domain):
	'''
	Web scraping from Athena search engine.
	The result content returned as a Spark DataFrame.

	:param input_text: the string to find a match for. E.g.: "Diabetes mellitus".
	:param domain: the desired domain of the input text. E.g.: "Condition","Procedure","Drug".
	'''

	search_text = input_text.replace(' ', '%20')
	size = 100 # set number of search suggestions
	search_url = f"https://athena.ohdsi.org/api/v1/concepts?domain={domain}&invalidReason=Valid&pageSize={size}&query={search_text}"
	html_page = requests.get(url=search_url, params={'User-Agent': 'Mozilla/5.0'}).text
	content = json.loads(html_page)['content']

	return content

if __name__ == "__main__":
	
	args = sys.argv[1:]
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", "--input", type=str, required=True, help="The desired item for search. E.g.: Diabetes mellitus")
	parser.add_argument("-o", "--output", type=str, required=True, help="The desired directory in which the results are saved")
	parser.add_argument("-d", "--domain", type=str, required=True, help="The desired domain. E.g.: Condition, Drug, Procedure")
	# parser.add_argument("-t", "--token", type=str, required=True, help="The access token file path for the ICD-11 API (client id and password)")

	args = parser.parse_args(args)

	input_text = args.input
	out_dir = args.output
	domain = args.domain
	# token = args.token

	# with open(token, mode='r', encoding='utf-8') as f:
	# 	credentials = json.load(f)
	# 	f.close()

	# # get the OAUTH2 token
	# payload = {'client_id': credentials["ClientId"],
	# 		'client_secret': credentials["ClientSecret"], 
	# 		'scope': 'icdapi_access', 
	# 		'grant_type': 'client_credentials'}

	# r = requests.post(credentials["tokenEndpoint"], data=payload, verify=True).json()
	# oauth2_token = r['access_token']

	# # access ICD API
	# uri = 'https://id.who.int/icd/entity'

	# # HTTP header fields to set
	# headers = {'Authorization':  'Bearer ' + oauth2_token, 
	# 		'Accept': 'application/json', 
	# 		'Accept-Language': 'en',
	# 		'API-Version': 'v2'}

	# # make request           
	# res = requests.get(uri, headers=headers, verify=True)

	# print(res.text)

	conf = SparkConf().setAppName("Athena Web Scraper")
	conf.set("spark.local.dir", "/home/gil/spark_shuffle")
	conf.set("spark.driver.memory", "10g")
	sc = SparkContext(conf=conf, master="local[*]")
	spark = SparkSession.builder.getOrCreate()

	# Create a schema for the dataframe
	schema = StructType([StructField('id', LongType(), False),
						StructField('code', StringType(), False),
						StructField('name', StringType(), False),
						StructField('className', StringType(), False),
						StructField('standardConcept', StringType(), False),
						StructField('invalidReason', StringType(), False),
						StructField('domain', StringType(), False),
						StructField('vocabulary', StringType(), False)])

	content = athena_web_scraper(input_text, domain)
	spark_df = spark.createDataFrame(data=content, schema=schema)
	spark_df = generateDataFrameKeys(spark, spark_df, colName="key")
	spark_df = spark_df.withColumn("lower_case_name", lower(spark_df["name"]))
	spark_df = spark_df.drop_duplicates(subset=["lower_case_name"])
	spark_df = spark_df.orderBy("key", ascending=True)
	spark_df = spark_df.drop("key","className","standardConcept","invalidReason","domain","lower_case_name")
	spark_df = spark_df.select("id","code","name",spark_df.vocabulary.alias("catalog"))
	spark_df = spark_df.limit(20)

	matched_diagnoses = [d.name for d in spark_df.select("name","code").collect()]
	for d1 in matched_diagnoses:
		for d2 in matched_diagnoses:
			if (d1 != d2) & (fuzz.ratio(d1, d2) / 100 >= 0.98):
				matched_diagnoses.remove(d2)

	spark_df = spark_df.where(spark_df.name.isin(matched_diagnoses))

	# save the results to disk
	spark_df.show(truncate=False)
	# writeDataFrameToFS(df=spark_df.coalesce(1), filePath=out_dir)
