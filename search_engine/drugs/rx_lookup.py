from os import system
import sys
import argparse
import numpy as np
import pandas as pd
from fuzzywuzzy import fuzz
import re
import json
from pandas.core.algorithms import value_counts
import requests

pd.set_option("display.max_columns", None)
pd.set_option("display.max_rows", None)
pd.set_option("max_colwidth",None)
pd.set_option("display.width", None)

clear = lambda: system('clear') # lambda function to clean unix terminal

# this script performs drug lookup inside RXTERM database

def show_available_drugs(rx, related_drugs):
	'''
	Display available drugs from RXTERM after filtering relevant drugs from free text search
	:rx param: The available RXTERM drug table
	:related_drugs: The relevant drugs filtered in free text as table format
	'''
	if len(related_drugs) == 0:
		print("No drugs found!")
		print("\nExiting.")
		return
	method = int(input("Please select search method:\n1. Lookup brand names\n2. Lookup generic names\n\n"))
	clear()

	if method == 1: # Branded drugs
		brand_df = related_drugs["BRAND_NAME"].dropna().drop_duplicates().sort_values(ascending=True).reset_index(drop=True)
		if len(brand_df) == 0:
			print("No brands found!")
			print("\nExiting.")
			return

		i = 0
		while True:
			print("Available brands:\n")
			for ind in brand_df.index.values[i:i+10]:
				print(ind, '\t', brand_df.loc[ind])

			brand_id = input("\nPlease select brand name or type 'm' for more options: ")
			if brand_id != "m":
				brand_id = brand_df.loc[int(brand_id)]
				clear()
				break
			i += 10
			clear()

		related_drugs = related_drugs.loc[related_drugs.BRAND_NAME == brand_id]

		# Drug route
		route_df = related_drugs["ROUTE"].drop_duplicates().reset_index(drop=True)
		print("Available routes:\n")
		for ind in route_df.index.values:
			print(ind, '\t', route_df.loc[ind])

		route_id = int(input("\nPlease select route: "))
		clear()
		route_id = route_df.loc[route_id]
		related_drugs = related_drugs.loc[related_drugs.ROUTE == route_id]

		# Drug dose form
		dose_form_df = related_drugs["RXN_DOSE_FORM"].drop_duplicates().reset_index(drop=True)
		print("Available dose forms:\n")
		for ind in dose_form_df.index.values:
			print(ind, '\t', dose_form_df.loc[ind])

		form_id = int(input("\nPlease select dose form: "))
		clear()
		form_id = dose_form_df.loc[form_id]
		related_drugs = related_drugs.loc[related_drugs.RXN_DOSE_FORM == form_id]

		# Drug strength
		strength_df = related_drugs["STRENGTH"].drop_duplicates().reset_index(drop=True)
		print("Available strengths:\n")
		for ind in strength_df.index.values:
			print(ind, '\t', strength_df.loc[ind])

		strength_id = int(input("\nPlease select strength: "))
		clear()
		strength_id = strength_df.loc[strength_id]

		# Drug prescribable and full names
		selected_drug = related_drugs.loc[related_drugs.STRENGTH == strength_id, ["PSN", "FULL_NAME"]]
		selected_drug.rename({"PSN":"Prescribable Name", "FULL_NAME":"Full Name"}, axis=1, inplace=True)

		print("Selected Drug:\n")
		for col in selected_drug.columns:
			print(col, ":", selected_drug[col].iloc[0])

	elif method == 2: # Generic drugs
		branded_drugs = rx.dropna(subset=["BRAND_NAME"]).copy()
		if len(branded_drugs) == 0:
			print("No brands found!")
			print("\nExiting.")
			return

		generic_drugs = related_drugs[related_drugs.TTY == "SCD"].copy()
		if len(generic_drugs) == 0:
			print("No drugs found!")
			print("\nExiting.")
			return

		i = 0
		while True:
			print("Available ingredients:\n")
			substances = generic_drugs['INGREDIENT'].drop_duplicates().sort_values().reset_index(drop=True).copy()
			for ind in substances.index.values[i:i+10]:
				print(ind, '\t', substances.loc[ind])

			generic_id = input("\nPlease select generic drug or type 'm' for more options: ")
			if generic_id != "m":
				generic_id = substances.loc[int(generic_id)]
				clear()
				break
			i += 10
			clear()

		related_drugs = generic_drugs.loc[generic_drugs["INGREDIENT"] == generic_id, ["RXCUI","TTY","ROUTE","RXN_DOSE_FORM","STRENGTH","FULL_NAME"]].copy()
		related_drugs.sort_values(by="ROUTE", ascending=True)
				
		# Drug route
		route_df = related_drugs["ROUTE"].drop_duplicates().reset_index(drop=True)
		print("Available routes:\n")
		for ind in route_df.index.values:
			print(ind, '\t', route_df.loc[ind])

		route_id = int(input("\nPlease select route: "))
		clear()
		route_id = route_df.loc[route_id]
		related_drugs = related_drugs.loc[related_drugs.ROUTE == route_id]

		# Drug dose form
		dose_form_df = related_drugs["RXN_DOSE_FORM"].drop_duplicates().reset_index(drop=True)
		print("Available dose forms:\n")
		for ind in dose_form_df.index.values:
			print(ind, '\t', dose_form_df.loc[ind])

		form_id = int(input("\nPlease select dose form: "))
		clear()
		form_id = dose_form_df.loc[form_id]
		related_drugs = related_drugs.loc[related_drugs.RXN_DOSE_FORM == form_id]

		# Drug strength
		strength_df = related_drugs["STRENGTH"].drop_duplicates().reset_index(drop=True)
		print("Available strengths:\n")
		for ind in strength_df.index.values:
			print(ind, '\t', strength_df.loc[ind])

		strength_id = int(input("\nPlease select strength: "))
		clear()
		strength_id = strength_df.loc[strength_id]

		# Drug full generic name
		clinical_drug = related_drugs.loc[related_drugs.STRENGTH == strength_id, "FULL_NAME"].iloc[0]

		# Drug prescribable and full names
		branded_drugs = branded_drugs.loc[branded_drugs.FULL_GENERIC_NAME == clinical_drug, ["PSN", "FULL_NAME"]].reset_index(drop=True)

		if len(branded_drugs) > 0:
			i = 0
			while True:
				print(f"Prescribable names related to {clinical_drug}:\n")
				for ind in branded_drugs.index.values[i:i+10]:
					print(ind, '\t', branded_drugs.loc[ind, "PSN"])

				brand_id = input("\nPlease select prescribable name or type 'm' for more options: ")
				if brand_id != "m":
					selected_drug = branded_drugs.loc[int(brand_id)]
					clear()
					break
				i += 10
				clear()

			print("Selected Drug:\n")
			print("Prescribable Name:", selected_drug.loc["PSN"])
			print("Full Name:", selected_drug.loc["FULL_NAME"])
		else:
			print("Brands not available!\n")
			print("Exiting.")

if __name__ == "__main__":
	
	args = sys.argv[1:]
	parser = argparse.ArgumentParser()
	parser.add_argument("-t", "--rxterms", type=str, required=True, help="The RxTerms TSV input file path")
	parser.add_argument("-c", "--rxclass", type=str, required=True, help="The RxClass TSV input file path")
	parser.add_argument("-r", "--termToClass", type=str, required=True, help="The term-class relationship TSV input file path")
	parser.add_argument("-in", "--ingredients", type=str, required=True, help="The term-ingredient relationship TSV input file path")

	args = parser.parse_args(args)
	rxterms_path = args.rxterms
	rxclass_path = args.rxclass
	termToClass_path = args.termToClass
	ingredient_path = args.ingredients

	schema = {"RXCUI": pd.Int64Dtype(),
			"GENERIC_RXCUI": pd.Int64Dtype(),
			"TTY": np.str,
			"FULL_NAME": np.str,
			"RXN_DOSE_FORM": np.str,
			"FULL_GENERIC_NAME": np.str,
			"BRAND_NAME": np.str,
			"DISPLAY_NAME": np.str,
			"ROUTE": np.str,
			"NEW_DOSE_FORM": np.str,
			"STRENGTH": np.str,
			"SUPPRESS_FOR": np.str,
			"DISPLAY_NAME_SYNONYM": np.str,
			"SXDG_RXCUI": pd.Int64Dtype(),
			"SXDG_TTY": np.str,
			"SXDG_NAME": np.str,
			"PSN": np.str}
			
	rx = pd.read_csv(rxterms_path, encoding="us-ascii", sep="\t").astype(schema)
	rx.replace(to_replace="nan", value=np.nan, inplace=True)

	clear()
	method = int(input("Please select search method:\n1. Search by brand name\n2. Search by generic name\n3. Search by symptom, disease or pharmacological class/action\n4. Search by prescribable name\n\n"))
	clear()

	if method == 1: # Search by Brand Name
		selected_brand = input("Please insert brand name for search: ").upper()
		clear()
		branded_drugs = rx.dropna(subset=["BRAND_NAME"]).copy()

		branded_drugs["fuzzy"] = branded_drugs["BRAND_NAME"].apply(lambda x: fuzz.ratio(x, selected_brand) / 100)
		related_drugs = branded_drugs[["RXCUI","TTY","BRAND_NAME","ROUTE","RXN_DOSE_FORM","STRENGTH","PSN","FULL_NAME","fuzzy"]].copy()
		related_drugs.sort_values(by=["fuzzy","BRAND_NAME","ROUTE","RXN_DOSE_FORM","STRENGTH"], ascending=False, inplace=True)

		# Drug brands
		brand_df = related_drugs["BRAND_NAME"].drop_duplicates().reset_index(drop=True)
		i = 0
		while True:
			print("Available brands:\n")
			for ind in brand_df.index.values[i:i+10]:
				print(ind, '\t', brand_df.loc[ind])

			brand_id = input("\nPlease select brand name or type 'm' for more options: ")
			if brand_id != "m":
				brand_id = brand_df.loc[int(brand_id)]
				clear()
				break
			i += 10
			clear()

		related_drugs = related_drugs.loc[related_drugs.BRAND_NAME == brand_id]

		# Drug route
		route_df = related_drugs["ROUTE"].drop_duplicates().reset_index(drop=True)
		print("Available routes:\n")
		for ind in route_df.index.values:
			print(ind, '\t', route_df.loc[ind])

		route_id = int(input("\nPlease select route: "))
		clear()
		route_id = route_df.loc[route_id]
		related_drugs = related_drugs.loc[related_drugs.ROUTE == route_id]

		# Drug dose form
		dose_form_df = related_drugs["RXN_DOSE_FORM"].drop_duplicates().reset_index(drop=True)
		print("Available dose forms:\n")
		for ind in dose_form_df.index.values:
			print(ind, '\t', dose_form_df.loc[ind])

		form_id = int(input("\nPlease select dose form: "))
		clear()
		form_id = dose_form_df.loc[form_id]
		related_drugs = related_drugs.loc[related_drugs.RXN_DOSE_FORM == form_id]

		# Drug strength
		strength_df = related_drugs["STRENGTH"].drop_duplicates().reset_index(drop=True)
		print("Available strengths:\n")
		for ind in strength_df.index.values:
			print(ind, '\t', strength_df.loc[ind])

		strength_id = int(input("\nPlease select strength: "))
		clear()
		strength_id = strength_df.loc[strength_id]

		# Drug prescribable and full names
		selected_drug = related_drugs.loc[related_drugs.STRENGTH == strength_id, ["PSN", "FULL_NAME"]]
		selected_drug.rename({"PSN":"Prescribable Name", "FULL_NAME":"Full Name"}, axis=1, inplace=True)

		print("Selected Drug:\n")
		for col in selected_drug.columns:
			print(col, ":", selected_drug[col].iloc[0])

	elif method == 2: # Search by Generic Name
		branded_drugs = rx.dropna(subset=["BRAND_NAME"]).copy()
		generic_drug = input("Please insert generic name for search: ").lower()
		clear()
		generic_drugs = rx[rx.TTY == "SCD"].copy()
		
		ingredients = pd.read_csv(ingredient_path, encoding='utf-8', sep='\t')
		ingredients['INGREDIENT'] = ingredients['INGREDIENT'].str.lower()
		substances = ingredients[['INGREDIENT']].drop_duplicates()
		substances["fuzzy"] = substances["INGREDIENT"].apply(lambda x: fuzz.ratio(x, generic_drug) / 100)
		substances = substances.sort_values(by=["fuzzy","INGREDIENT"], ascending=[False,True])
		substances = substances["INGREDIENT"].reset_index(drop=True)

		# Generic drugs
		i = 0
		while True:
			print("Available ingredients:\n")
			for ind in substances.index.values[i:i+10]:
				print(ind, '\t', substances.loc[ind])

			generic_id = input("\nPlease select generic drug or type 'm' for more options: ")
			if generic_id != "m":
				generic_id = substances.loc[int(generic_id)]
				clear()
				break
			i += 10
			clear()

		related_drugs = generic_drugs.merge(ingredients[ingredients["INGREDIENT"] == generic_id], on=['RXCUI'], how='inner')
		related_drugs = related_drugs[["RXCUI","TTY","ROUTE","RXN_DOSE_FORM","STRENGTH","FULL_NAME"]].copy()
		related_drugs.sort_values(by="ROUTE", ascending=True)
		
		# Drug route
		route_df = related_drugs["ROUTE"].drop_duplicates().reset_index(drop=True)
		print("Available routes:\n")
		for ind in route_df.index.values:
			print(ind, '\t', route_df.loc[ind])

		route_id = int(input("\nPlease select route: "))
		clear()
		route_id = route_df.loc[route_id]
		related_drugs = related_drugs.loc[related_drugs.ROUTE == route_id]

		# Drug dose form
		dose_form_df = related_drugs["RXN_DOSE_FORM"].drop_duplicates().reset_index(drop=True)
		print("Available dose forms:\n")
		for ind in dose_form_df.index.values:
			print(ind, '\t', dose_form_df.loc[ind])

		form_id = int(input("\nPlease select dose form: "))
		clear()
		form_id = dose_form_df.loc[form_id]
		related_drugs = related_drugs.loc[related_drugs.RXN_DOSE_FORM == form_id]

		# Drug strength
		strength_df = related_drugs["STRENGTH"].drop_duplicates().reset_index(drop=True)
		print("Available strengths:\n")
		for ind in strength_df.index.values:
			print(ind, '\t', strength_df.loc[ind])

		strength_id = int(input("\nPlease select strength: "))
		clear()
		strength_id = strength_df.loc[strength_id]

		# Drug full generic name
		clinical_drug = related_drugs.loc[related_drugs.STRENGTH == strength_id, "FULL_NAME"].iloc[0]

		# Drug prescribable and full names
		branded_drugs = branded_drugs.loc[branded_drugs.FULL_GENERIC_NAME == clinical_drug, ["PSN", "FULL_NAME"]].reset_index(drop=True)

		if len(branded_drugs) > 0:
			i = 0
			while True:
				print(f"Prescribable names related to {clinical_drug}:\n")
				for ind in branded_drugs.index.values[i:i+10]:
					print(ind, '\t', branded_drugs.loc[ind, "PSN"])

				brand_id = input("\nPlease select prescribable name or type 'm' for more options: ")
				if brand_id != "m":
					selected_drug = branded_drugs.loc[int(brand_id)]
					clear()
					break
				i += 10
				clear()

			print("Selected Drug:\n")
			print("Prescribable Name:", selected_drug.loc["PSN"])
			print("Full Name:", selected_drug.loc["FULL_NAME"])
		else:
			print("Brands not available!\n")
			print("Exiting.")

	elif method == 3: # Search by mechanism of action / pharmacological class or action / symptom / disease / anatomical therapeutic chemical [ATC] / substance(s) / Disposition / Structure
		rxToClass = pd.read_csv(termToClass_path, encoding='utf-8', sep='\t')
		rx_classes = pd.read_csv(rxclass_path, encoding='utf-8', sep='\t')
		uniqueRxClasses = rx_classes.copy().drop_duplicates(subset=["className"])

		search_phrase = input("Please insert text for search: ").lower()
		clear()
		uniqueRxClasses['fuzzy'] = uniqueRxClasses['className'].apply(lambda x: fuzz.ratio(x, search_phrase) / 100)
		uniqueRxClasses.sort_values(by="fuzzy", ascending=False, inplace=True)
		uniqueRxClasses.reset_index(drop=True, inplace=True)

		i = 0
		while True:
			print(f"Classes related to '{search_phrase}':\n")
			for ind in uniqueRxClasses.index.values[i:i+10]:
				print(ind, '\t', uniqueRxClasses.loc[ind, "className"])

			class_id = input("\nPlease select relevant class or type 'm' for more options: ")
			if class_id != "m":
				selected_class_id = uniqueRxClasses.loc[int(class_id), "classId"]
				selected_class_name = uniqueRxClasses.loc[int(class_id), "className"]
				clear()
				break
			i += 10
			clear()

		while True:
			relevant_class_names = rx_classes.loc[rx_classes.className == selected_class_name, "classType"].unique()
			relevant_class_ids = rx_classes.loc[rx_classes.className == selected_class_name, "classId"].unique()
			relevant_class_codes = [x.split('-')[-1] for x in relevant_class_ids]
			sub_classes = []
			for cl in relevant_class_codes:
				url = f'https://rxnav.nlm.nih.gov//REST/rxclass/classTree.json?classId={cl}'
				page = requests.get(url, {'User-Agent':'Mozilla/5.0'}).text
				content = json.loads(page)['rxclassTree'][0]
				if 'rxclassTree' in content.keys():
					children = content['rxclassTree']
					for child in children:
						sub_classes.append(child['rxclassMinConceptItem']['className'].lower())
				else:
					related_drugs = rx.merge(rxToClass[rxToClass.RXCLASS.isin(relevant_class_ids)], on=["RXCUI"], how='inner').drop_duplicates(subset=["RXCUI"])
					related_drugs = related_drugs[['RXCUI','FULL_NAME','TTY','BRAND_NAME','ROUTE','RXN_DOSE_FORM','STRENGTH','PSN']].copy()
					if cl != relevant_class_codes[-1]:
						continue
					clear()
					show_available_drugs(rx, related_drugs)
					break

			sub_classes = pd.DataFrame(sub_classes, columns=['Class']).drop_duplicates().sort_values(by=['Class'], ascending=True)
			sub_classes.reset_index(drop=True, inplace=True)
			i = 0
			while True:
				print(f"Sub-classes related to '{selected_class_name}':\n")
				for ind in sub_classes.index.values[i:i+10]:
					print(ind, '\t', sub_classes.loc[ind, "Class"])

				class_id = input(f"\nPlease select relevant class (type 'm' for more options) or type 'd' to show drugs related to '{selected_class_name}': ")
				if class_id == 'd':
					related_drugs = rx.merge(rx_classes[rx_classes.className == selected_class_name].rename({'classId':'RXCLASS'}, axis=1).merge(rxToClass, on=['RXCLASS'], how='inner'), on=["RXCUI"], how='inner')[['RXCUI','FULL_NAME','TTY','BRAND_NAME','ROUTE','RXN_DOSE_FORM','STRENGTH','PSN','RXCLASS']].copy()
					related_drugs.drop_duplicates(subset=["RXCUI"], inplace=True)
					clear()
					show_available_drugs(rx, related_drugs)
					break
				elif class_id != 'm':
					selected_class_name = sub_classes.loc[int(class_id), "Class"]
					selected_class_id = rx_classes.loc[rx_classes.className == selected_class_name, "classId"].tolist()
					clear()
					break
				i += 10
				clear()
			if class_id == 'd':
				break
	elif method == 4: # Search by prescribable name
		search_phrase = input("Please insert text for search: ").lower()
		clear()

		keywords = set(search_phrase.split(' '))
		rx = rx[rx.BRAND_NAME.notna()]
		rx["score"] = rx["PSN"].apply(lambda x: len(set(x.split(' ')).intersection(keywords)))
		rx = rx[rx.score >= 2]
		rx.sort_values(by="score", ascending=False, inplace=True)
		rx.reset_index(drop=True, inplace=True)

		i = 0
		while True:
			print(f"Prescribable drug names related to '{search_phrase}':\n")
			for ind in rx.index.values[i:i+10]:
				print(ind, '\t', rx.loc[ind, "PSN"])

			drug_id = input("\nPlease select relevant drug or type 'm' for more options: ")
			if drug_id != "m":
				clear()
				print("Selected Drug:\n")
				print("Prescribable Name:", rx.loc[int(drug_id), "PSN"])
				print("Full Name:", rx.loc[int(drug_id), "FULL_NAME"])
				break
			i += 10
			clear()
			if i > len(rx):
				print("No more options found!")
				print("\nExiting.")
				break
