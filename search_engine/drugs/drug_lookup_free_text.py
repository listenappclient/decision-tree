from collections import OrderedDict
import string
import json
import re
import time
from fuzzywuzzy import fuzz
import pandas as pd
import spacy
import nltk
from nltk.corpus import stopwords
nltk.download('punkt')
nltk.download('stopwords')
from nltk.tokenize import word_tokenize

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)

# this script perdforms a druglookup from free text using NLP methods

# Load English tokenizer, tagger, parser and NER
nlp = spacy.load("en_core_web_sm")

# customize stopwords and translator
stop_words = stopwords.words('english') + ['include','includes','therapy','treatment','treat','treats','indicate','indicates','indicated','ment','however','combination','combine','combines','clinical']
translator = {"type ii":"type 2", "ii type":"2 type", "i type":"1 type", "type i":"type 1", "on-set":"onset", "aemia": "emia", "aesia": "esia", "HIV": "immunodeficiency virus"}

with open('/home/gil/files/idr_database.json', mode='r') as f:
	drugs = json.load(f)
	f.close()


def pipe(text, translator):
	# lower cases
	text = text.lower()
	# remove special characters characters
	text = text.translate(str.maketrans('', '', string.punctuation))
	# translate specific medical terms
	for s in translator.keys():
		text = text.replace(s, translator[s])
	# remove stop words from text
	text_tokens = word_tokenize(text)
	text = ' '.join([word for word in text_tokens if not word in stop_words])
	# include only lemmatized verbs, nouns and entities only
	doc = nlp(text)
	verbs = [token.lemma_ for token in doc if token.pos_ == "VERB"]
	all_verbs = []
	for verb in verbs:
		all_verbs += verb.split(' ')
	nouns = [token.text for token in doc if token.pos_ == "NOUN"]
	all_nouns = []
	for noun in nouns:
		all_nouns += noun.split(' ')
	adjectives = [token.lemma_ for token in doc if token.pos_ == "ADJ"]
	entities = [entity.text for entity in doc.ents]
	all_ents = []
	for ent in entities:
		all_ents += ent.split(' ')
	# tags = list(OrderedDict.fromkeys(all_verbs + all_nouns + entities)) keep the order
	tags = sorted(list(set(all_verbs + all_nouns + entities)))
	text = ' '.join(tags)
	return text, tags


input_text = "Treats diabetes mellitus, of type II"

search_text, tags = pipe(text=input_text, translator=translator)
indications = {drug_id: pipe(text=drugs[drug_id]["approvedIndication"], translator=translator)[0] for drug_id in drugs.keys()}
matches = []

# # removing all digits from text
# text = re.sub('[\d-]', '', text).replace('  ',' ')


for drug_id in indications.keys():
	ind = indications[drug_id]

	# if all([s in ind for s in tags]):
	fuzzy_score = fuzz.ratio(search_text, ind) / 100
	matches.append({"drug_id":drug_id,
				 "brand_name_en":drugs[drug_id]["brandName"]["en"],
				 "brand_name_he":drugs[drug_id]["brandName"]["he"],
				 "indication":drugs[drug_id]["approvedIndication"],
				 "tags":ind,
				 "fuzzy":fuzzy_score})
	

lookup_tbl = pd.DataFrame(matches)
lookup_tbl = lookup_tbl[lookup_tbl.fuzzy >= 0.6]
lookup_tbl.set_index("drug_id", drop=True, inplace=True)
lookup_tbl.sort_values(by=["fuzzy"], ascending=[False], inplace=True)



for i in lookup_tbl.index:
	print(f"\n\n{lookup_tbl.loc[i,'brand_name_en']}\n",lookup_tbl.loc[i,"tags"])
	time.sleep(10)
# id = 10309



