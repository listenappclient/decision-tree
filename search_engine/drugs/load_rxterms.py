import os
import sys
from time import time
import shutil
import re
import json
from argparse import ArgumentParser
from zipfile import ZipFile
import requests
import numpy as np
import pandas as pd
import sqlalchemy
import pymysql
from ast import literal_eval
import logging

# this script downlaods, extracts, transforms, and loads the RXTERM data (from RXNORM database) into a designated SQL database.

args = sys.argv[1:]
parser = ArgumentParser()

parser.add_argument("-v", "--version", type=str, required=False, help="The version of RxTerms in 'YYYYMM' format.")
parser.add_argument("-d", "--dir", type=str, required=False, help="The absulute path of the directory in which the RxTerms files will be downloaded to.")

args = parser.parse_args(args)
ver = args.version
rxterm_path = args.dir


rx_schema = {"RXCUI": pd.Int64Dtype(),
			"GENERIC_RXCUI": pd.Int64Dtype(),
			"TTY": np.str,
			"FULL_NAME": np.str,
			"RXN_DOSE_FORM": np.str,
			"FULL_GENERIC_NAME": np.str,
			"BRAND_NAME": np.str,
			"DISPLAY_NAME": np.str,
			"ROUTE": np.str,
			"NEW_DOSE_FORM": np.str,
			"STRENGTH": np.str,
			"SUPPRESS_FOR": np.str,
			"DISPLAY_NAME_SYNONYM": np.str,
			"IS_RETIRED": np.str,
			"SXDG_RXCUI": pd.Int64Dtype(),
			"SXDG_TTY": np.str,
			"SXDG_NAME": np.str,
			"PSN": np.str}

ingredient_schema = {"RXCUI": pd.Int64Dtype(),
					"INGREDIENT": np.str,
					"ING_RXCUI": pd.Int64Dtype()}

url = f'https://data.lhncbc.nlm.nih.gov/public/rxterms/release/RxTerms{ver}.zip'
r = requests.get(url, allow_redirects=True)

# download latest release of RxTerms as ZIP file
file_path = f'{rxterm_path}/RxTerms{ver}.zip'
open(file_path, 'wb').write(r.content)

# Unzip the downloaded file
with ZipFile(file_path, 'r') as zip_file:
	zip_file.extractall(rxterm_path)

# Remove unneccessary files
for filename in os.listdir(rxterm_path):
	if filename in [f'RxTerms{ver}.txt',f'RxTermsIngredients{ver}.txt']:
		continue
	else:
		filename_path = os.path.join(rxterm_path, filename)
		os.unlink(filename_path)

# create a log file and record all outputs
logging.basicConfig(filename=f'{rxterm_path}/logfile_{int(time())}.log',
				 level=logging.INFO,
				 format='%(asctime)s - %(levelname)s:%(message)s',
				 filemode='w')
logger = logging.getLogger()

# Process the data
rx_ingredient = pd.read_csv(f'{rxterm_path}/RxTermsIngredients{ver}.txt', encoding='us-ascii', sep='|').astype(ingredient_schema)
rx_ingredient['INGREDIENT'] = rx_ingredient['INGREDIENT'].str.lower()

rx = pd.read_csv(f'{rxterm_path}/RxTerms{ver}.txt', encoding='us-ascii', sep='|').astype(rx_schema)
rx.drop(columns=['IS_RETIRED'], inplace=True)
rx.replace(to_replace='nan', value=np.nan, inplace=True)
na_psn = rx[rx.PSN.isna()].index
rx.loc[na_psn, "PSN"] = rx.loc[na_psn,"FULL_NAME"]
rx['BRAND_NAME'] = rx["BRAND_NAME"].str.upper()
rx["PSN"] = rx["PSN"].str.lower()
rx["FULL_NAME"] = rx["FULL_NAME"].str.lower()
rx["FULL_GENERIC_NAME"] = rx["FULL_GENERIC_NAME"].str.lower()
rx["SUBSTANCE(S)"] = rx["FULL_NAME"].apply(lambda x: re.sub(repl='', string=x, pattern='\[.*\]'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'((\d+\.{0,}\d+\s+)|(\d+\s+))(mg\sdoses|mg\sdose|unt\sdoses|unt\sdose|mg\/ml|mg\/mg|mg\/hr|mg\/actuat|mg\/sqcm|unt\/ml|cells\/ml|mcg\/actuat|ml\/actuat|ml\/ml|unt\/actuat|unt\/mg|unt\/ml|meq\/ml|meq\/mcg|meq\/mg|mci\/ml|vector-genomes\/ml|bau\/ml|mg|mcg|ml|mci|unt|actuat|meq|sqcm|hr|ir|day|%|vector-genomes|cells|bau)'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'\d+\-month'))

forms = []
for form in rx['RXN_DOSE_FORM'].unique():
	forms.append({"form":form, "len":len(form)})
forms = pd.DataFrame(forms).sort_values(by="len", ascending=False)
forms = forms['form'].tolist()
for form in forms:
	rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: x.replace(form.lower(), '').replace('/ /','/').replace('./','/'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl=' ', string=x, pattern=r'\s+'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'^\s+|\s+$'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'(\{\d+\s+\(\s{0,})|(\)\s+\})'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'\s{0,}\)\s+\/\s+\d+\s+(\(\)\s+){0,}\('))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'^\)\s{0,}\('))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'\d+\-bead'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'\d+\/\d+\s+release\s+'))
rx["SUBSTANCE(S)"] = rx["SUBSTANCE(S)"].apply(lambda x: re.sub(repl='', string=x, pattern=r'^\s+|^,\s{0,}|\s+$'))

# Rxterms to Ingredient relationship table
substances = []
for i, row in rx.iterrows():
	rxcui = row["RXCUI"]
	substances += [{"RXCUI":rxcui, "INGREDIENT":x} for x in row["SUBSTANCE(S)"].split(' / ')]
substances = pd.DataFrame(substances).drop_duplicates().reset_index(drop=True)
rx.drop(columns=["SUBSTANCE(S)"], inplace=True)

# Rxterms to RxClass relationship table
rx_class = []
for rxcui in rx.RXCUI.values:
	url = f'https://rxnav.nlm.nih.gov/REST/rxclass/class/byRxcui.json?rxcui={rxcui}'
	r = requests.get(url=url, params={'User-Agent':'Mozilla/5.0'})
	try:
		records = json.loads(r.text)['rxclassDrugInfoList']['rxclassDrugInfo']
	except KeyError:
		continue
	for rec in records:
		related_class = rec['rxclassMinConceptItem']
		class_id =  related_class['classId']
		class_type = related_class['classType']
		rx_class.append({'RXCUI':rxcui, 'RXCLASS':'-'.join([class_type,class_id])})
rx_class = pd.DataFrame(rx_class).drop_duplicates().sort_values(by=["RXCUI","RXCLASS"], ascending=True).reset_index(drop=True)

# RxClass table
rx_classes_json = []
class_names = ['ATC1-4','EPC','MESHPA','DISEASE','CHEM','MOA','PE','PK','VA','TC','DISPOS','STRUCT','SCHEDULE']
class_codes = ['0','N0000189939','D020228','X1','Z1','N0000000223','N0000009802','N0000000003','VA000','N0000178293','766779001','763760008','SCHEDULE0']
for class_type, class_id in zip(class_names, class_codes):
	url = f'https://rxnav.nlm.nih.gov/REST/rxclass/classTree.json?classType={class_type}&classId={class_id}'
	r = requests.get(url=url, params={"User-Agent":"Mozilla/5.0"})
	content = json.loads(r.text.replace('<n>','').replace('</n>','').replace('<o>','').replace('</o>',''))
	class_tree = content['rxclassTree'][0]
	rx_classes_json.append(class_tree)

rx_classes_json = {"Rx_classes": rx_classes_json}
with open(f'{rxterm_path}/RxClasses{ver}.json', mode='w', encoding='utf-8') as outfile:
	json.dump(obj=rx_classes_json, fp=outfile, indent='\t')

# flat json tree to CSV
rx_classes_json = str(rx_classes_json)
records = [literal_eval('{' + rec.split('{',2)[-1]) for rec in re.findall("\{'rxclassMinConceptItem.*?\}", rx_classes_json)]
rxclass_df = pd.DataFrame(records).drop_duplicates()
rxclass_df['className'] = rxclass_df['className'].str.lower()
rxclass_df['classId'] = rxclass_df['classType'] + '-' + rxclass_df['classId']
classKeyMaxLen = rxclass_df.classId.str.len().max() # save the maximum length of the class ids

# write to local drive as CSV files seperated by tabs
rx.to_csv(f'{rxterm_path}/RxTerms{ver}.csv', encoding='us-ascii', sep='\t', index=False)
substances.to_csv(f'{rxterm_path}/RxCompleteIngredients{ver}.csv', encoding='us-ascii', sep='\t', index=False)
rx_ingredient.to_csv(f'{rxterm_path}/RxTermsIngredients{ver}.csv', encoding='us-ascii', sep='\t', index=False)
rx_class.to_csv(f'{rxterm_path}/RxTermsClasses{ver}.csv', encoding='us-ascii', sep='\t', index=False)
rxclass_df.to_csv(f'{rxterm_path}/RxClasses{ver}.csv', encoding='utf-8', sep='\t', index=False)

# remove old files from disk
os.remove(f'{rxterm_path}/RxTerms{ver}.txt')
os.remove(f'{rxterm_path}/RxTermsIngredients{ver}.txt')

# adding key fields for the relationship tables
substances.reset_index(drop=False, inplace=True)
substances.rename({"index": "ID"}, axis=1, inplace=True)

rx_class.reset_index(drop=False, inplace=True)
rx_class.rename({"index": "ID"}, axis=1, inplace=True)

# connect to MySQL server and database
logger.info("Connecting to MySQL server and database...")
print("\nConnecting to MySQL server and database...")
sqlEngine = sqlalchemy.create_engine('mysql+pymysql://root:Th3Spidr160191$@127.0.0.1/test', pool_recycle=3600)

try:
	dbConnection = sqlEngine.connect()
	print("\nConnection established.")
	logger.info("Connection established.")
	print("\nCreating and loading tables into MySQL Server...")
	logger.info("Creating and loading tables into MySQL Server...")
	rx.to_sql(name='RxTerm', con=dbConnection, if_exists='replace', index=False, index_label="RXCUI")
	rxclass_df.to_sql(name='RxClass', con=dbConnection, if_exists='replace', index=False, index_label="classId")
	substances.to_sql(name="RxTermIngredient", con=dbConnection, if_exists='replace', index=False)
	rx_class.to_sql(name="RxTermClass", con=dbConnection, if_exists='replace', index=False)
except ValueError as vx:
	print("\n",vx)
	logger.error(vx)
except Exception as ex:   
	print("\n",ex)
	logger.error(ex)
else:
	print("\nTables created successfully.\n\n")
	logger.info("Tables created successfully.")
finally: # disconnect from MySQL server
	dbConnection.close()


# connect to MySQL server and database again
try:
	dbConnection = pymysql.connect(host='localhost', user='root', password='Th3Spidr160191$', database='test')
	with dbConnection.cursor() as cur:
		# define the maximum row id sizes before setting to primary/foreign key
		cur.execute(f'alter table RxClass modify classId VARCHAR({classKeyMaxLen})')
		cur.execute(f'alter table RxTermClass modify RXCLASS VARCHAR({classKeyMaxLen})')

		# define primary and foreign keys
		for tbl, ind in zip(['RxTerm','RxClass','RxTermIngredient','RxTermClass'], ['RXCUI','classId','ID','ID']):
			cur.execute(f'alter table {tbl} add primary key({ind})')
			if tbl == 'RxTermIngredient':
				cur.execute(f'alter table {tbl} add foreign key(RXCUI) references RxTerm(RXCUI)')
			elif tbl == 'RxTermClass':
				cur.execute(f'alter table {tbl} add foreign key(RXCUI) references RxTerm(RXCUI)')
				cur.execute(f'alter table {tbl} add foreign key(RXCLASS) references RxClass(classId)')
		cur.fetchall()

		for tbl in ['RxTerm','RxClass','RxTermIngredient','RxTermClass']:
			cur.execute(f"describe {tbl}")
			result = cur.fetchall()
			result = pd.DataFrame(result, columns=["Field", "Type", "Null", "Key", "Default", "Extra"]).fillna({"Default":"NULL"}).set_index("Field")
			print(f"'{tbl}' table description:\n")
			logger.info(f"'{tbl}' table description:")
			print(result)
			logger.info(result)

			print(f"\nShowing 10 first rows:\n")
			logger.info("Showing 10 first rows:")
			cur.execute(f"select * from {tbl} limit 10")
			result = cur.fetchall()
			for row in result:
				print(row)
				logger.info(row)
			print("\n\n","="*100,"\n\n")
			logger.info("="*100)
except ValueError as vx:
	print("\n",vx)
	logger.error(vx)
except Exception as ex:   
	print("\n",ex)
	logger.error(ex)
else:
	print("\nPrimary and foreign keys created successfully.\n\n")
	logger.info("Primary and foreign keys created successfully.")
	print("Done!")
	logger.info("Done!")
finally:
	print("\nDisconnecting from MySQL server...")
	logger.info("Disconnecting from MySQL server...")
	dbConnection.close()
	print("\nDisconnected.")
	logger.info("Disconnected.")
