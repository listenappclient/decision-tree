import sys
import argparse
import json
import requests
from bs4 import BeautifulSoup
from fuzzywuzzy import fuzz
from strsimpy.damerau import Damerau
import pandas as pd
from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, LongType
from pyspark.sql.functions import lower

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)


# this scripts performs diagnosis (dx) or procedure (pr) inside ICD-11 MMS and OHDSI Athena database

def athena_web_scraper(input_text, domain, page_size=100):
	'''
	Web scraping from Athena search engine.
	The result content returned as a Spark DataFrame.

	:param input_text: the string to find a match for. E.g.: "Diabetes mellitus" (required).
	:param domain: the desired domain of the input text. E.g.: "Condition","Procedure","Drug" (required).
	:param page_size: number of search suggestions (optional). Default to 100.
	'''

	search_text = input_text.replace(' ', '%20')
	search_url = f"https://athena.ohdsi.org/api/v1/concepts?domain={domain}&invalidReason=Valid&pageSize={page_size}&query={search_text}"
	html_page = requests.get(url=search_url, params={'User-Agent': 'Mozilla/5.0'}).text
	content = json.loads(html_page)['content']

	return content

def generateDataFrameKeys(spark, df, offset=1, colName="id"):
	'''
		Enumerates dataframe rows is native order, like rdd.ZipWithIndex(), but on a dataframe
		and preserves a schema

		:param df: source dataframe
		:param offset: adjustment to zipWithIndex()'s index
		:param colName: name of the index column
	'''

	zipped_rdd = df.rdd.zipWithIndex() # convert to RDD and generate new keys

	new_schema = StructType(
		[StructField(colName,LongType(),True)]        # new added field in front
		+ df.schema.fields)                            # previous schema

	new_rdd = zipped_rdd.map(lambda args: ([args[1] + offset] + list(args[0])))      # use this for python 3+, tuple gets passed as single argument so using args and [] notation to read elements within args
	new_df = spark.createDataFrame(new_rdd, new_schema)

	return new_df

def get_children(id, source, adopted_children=True):
	'''
		Gets diagnosis children info.

			:param id (int): ICD-11 foundation id or athena concept_id as a root node (required).
			:param source (str): Source of the root id. Available values are 'icd-11' or 'athena' (required).
			:param adopted_children (bool): When set to True, include root node adopted children (default to True, only relevant when source='icd-11').
	'''
	
	res = []
	if source == 'icd-11':
		if adopted_children:
			adopted_children = 'true'
		else:
			adopted_children = 'false'
		if (id.endswith('other')) | (id.endswith('unspecified')):
			id = id.replace('.','/mms/')
		url = f'https://icd.who.int/browse11/l-m/en/JsonGetChildrenConcepts?ConceptId=http://id.who.int/icd/entity/{id}&useHtml=true&showAdoptedChildren={adopted_children}'
		while True:
			try:
				page = requests.get(url=url, timeout=30, params={"User-Agent":'Mozilla/5.0'}).text
				break
			except requests.ConnectionError as e:
				print("\nOOPS!! Connection error. Make sure you are connected to Internet. Technical details given below.\n")
				print(str(e))
				continue
			except requests.Timeout as e:
				print("\nOOPS!! Timeout error. Technical details given below.\n")
				print(str(e))
				continue
			except requests.RequestException as e:
				print("\nOOPS!! General error. Technical details given below.\n")
				print(str(e))
				continue
		try:
			children = json.loads(page)
		except json.JSONDecodeError:
			children = []
		if len(children) == 0:
			print("Could not find a more specific diagnosis..")
			return res
		
		for child in children:
			child_id = child['ID'].split('/')[-1]
			page = BeautifulSoup(child['html'], 'html.parser')
			child_code = page.span.text
			if child_code in ['','(TM1)']:
				if child_code == '':
					child_desc = page.text.split('\r\n')[1].split('\n')[0]
				elif child_code == '(TM1)':
					child_desc = page.text.split('\r\n')[5].split('\n')[0]
				child_code = None
			else:
				child_desc = page.text.split('\r\n')[1].split('\n')[0]
			if (child_id == "unspecified") & (not child_desc.endswith('unspecified')):
				child_desc = child_desc + ', unspecified'
			res.append({"diagnosis": child_desc, "foundation":child_id})

	elif source == 'athena':
		url = f'https://athena.ohdsi.org/api/v1/concepts/{id}/relationships?std=false'
		while True:
			try:
				page = requests.get(url=url, timeout=30, params={"User-Agent":'Mozilla/5.0'}).text
				break
			except requests.ConnectionError as e:
				print("\nOOPS!! Connection error. Make sure you are connected to Internet. Technical details given below.\n")
				print(str(e))
				continue
			except requests.Timeout as e:
				print("\nOOPS!! Timeout error. Technical details given below.\n")
				print(str(e))
				continue
			except requests.RequestException as e:
				print("\nOOPS!! General error. Technical details given below.\n")
				print(str(e))
				continue
		children = json.loads(page)
		if children['count'] == 0:
			return res
		for relation in children['items']:
			if relation['relationshipName'] == 'Subsumes':
				for child in relation['relationships']:
					res.append({"id": child['targetConceptId'], "diagnosis": child['targetConceptName'], "catalog": child['targetVocabularyId']})

	return res


if __name__ == "__main__":
	
	args = sys.argv[1:]
	parser = argparse.ArgumentParser()
	parser.add_argument("-icd", "--icd_table", type=str, required=True, help="The ICD table as TSV file")
	parser.add_argument("-tree", "--icd_tree", type=str, required=False, help="The ICD tree as JSON file")
	parser.add_argument("-in", "--input", type=str, required=True, help="The desired item for search. E.g.: Diabetes mellitus")

	args = parser.parse_args(args)

	icd_table = args.icd_table
	icd_tree = args.icd_tree
	input_text = args.input

	conf = SparkConf().setAppName("Athena Web Scraper")
	conf.set("spark.local.dir", "/home/gil/spark_shuffle")
	conf.set("spark.driver.memory", "10g")
	sc = SparkContext(conf=conf, master="local[*]")
	spark = SparkSession.builder.getOrCreate()

	schema = StructType([StructField('id', LongType(), False),
			StructField('code', StringType(), False),
			StructField('name', StringType(), False),
			StructField('className', StringType(), False),
			StructField('standardConcept', StringType(), False),
			StructField('invalidReason', StringType(), False),
			StructField('domain', StringType(), False),
			StructField('vocabulary', StringType(), False)])

	diagnoses = pd.read_csv(icd_table, sep="\t")
	with open(icd_tree, mode='r') as json_file:
		icd_11_cat = json.load(json_file)
		json_file.close()

	matches = []
	damerau = Damerau()

	for d in diagnoses.title.values:
		fuzzy_score = fuzz.ratio(input_text, d) / 100

		matches.append({"diagnosis":d, "fuzzy":fuzzy_score})

	lookup_tbl = pd.DataFrame(matches)

	lookup_tbl.set_index("diagnosis", drop=True, inplace=True)
	diagnoses.set_index("title", drop=True, inplace=True)
	diagnoses = diagnoses.loc[lookup_tbl.index]

	lookup_tbl = pd.concat([lookup_tbl, diagnoses], axis=1)
	lookup_tbl.sort_values(by=["fuzzy"], ascending=[False], inplace=True)
	lookup_tbl = lookup_tbl.iloc[:1000]

	decision = []
	matches = []
	for d in lookup_tbl.index.values:
		fuzzy_score = lookup_tbl.loc[d,"fuzzy"]
		damerau_score = damerau.distance(input_text, d) / 100
		final_score = 0.9 * fuzzy_score - 0.1*damerau_score
		matches.append({"foundation":lookup_tbl.loc[d,"foundation"], "code":lookup_tbl.loc[d,"code"], "diagnosis":d, "fuzzy":fuzzy_score, "damerau":damerau_score, "score":final_score})

	lookup_tbl = pd.DataFrame(matches)
	# lookup_tbl.sort_values(by=["fuzzy","damerau"], ascending=[False,True], inplace=True)
	lookup_tbl.sort_values(by=["score","diagnosis"], ascending=[False,True], inplace=True)
	lookup_tbl = lookup_tbl.iloc[:20]
	lookup_tbl.reset_index(drop=True, inplace=True)

	print("\n\nMost relevant choices for " + input_text + ":")
	print(lookup_tbl[["diagnosis","score"]])

	cnt = 0
	flag = False
	break_flag = False
	while True:
		if len(lookup_tbl) > 0:
			selection = input("\n\nPlease choose your selection (type 'O' for other suggestions): ")
			if selection not in [str(i) for i in lookup_tbl.index.values] + ['O']:
					print("Oops! Invalid input detected. Let's try again...")
					continue
			try:
				selection = int(selection)
				selected_diagnosis = lookup_tbl.loc[selection, "diagnosis"]
				if "foundation" in lookup_tbl.columns:
					selected_id = lookup_tbl.loc[selection, "foundation"]
				elif "id" in lookup_tbl.columns:
					selected_id = lookup_tbl.loc[selection, "id"]
			except ValueError:
				if selection == 'O':
					selected_diagnosis = "Other specified"
				else:
					print("Oops! Invalid input detected. Let's try again...")
					continue
		else:
			selected_diagnosis = "Other specified"

		if selected_diagnosis.startswith("Other specified"):
			if len(decision) > 0:
				selected_diagnosis = decision[-1]

			if flag:
				print(f"Diagnosis '{selected_diagnosis}' has been selected because no other suggestions were found...")
				print("\nExiting.")
				break
			else:
				if  cnt == 0 :
					selected_diagnosis = input_text
				else:
					flag = True

			print("\nSearching for more specific diagnoses in our database...\n")
			content = athena_web_scraper(selected_diagnosis, "Condition")
			spark_df = spark.createDataFrame(data=content, schema=schema)
			spark_df = generateDataFrameKeys(spark, spark_df, colName="key")
			spark_df = spark_df.withColumn("lower_case_name", lower(spark_df["name"]))
			spark_df = spark_df.drop_duplicates(subset=["lower_case_name"])
			spark_df = spark_df.orderBy("key", ascending=True)
			spark_df = spark_df.drop("key","className","standardConcept","invalidReason","domain","lower_case_name")
			spark_df = spark_df.select("id","code",spark_df.name.alias("diagnosis"),spark_df.vocabulary.alias("catalog"))
			if len(lookup_tbl) > 0:
				spark_df = spark_df.where(~spark_df.diagnosis.isin(lookup_tbl["diagnosis"].tolist()))
			else:
				spark_df = spark_df.where(spark_df.diagnosis != selected_diagnosis)
			spark_df = spark_df.limit(20)

			matched_diagnoses = [d.diagnosis for d in spark_df.select("diagnosis","code").collect()]
			for d1 in matched_diagnoses:
				for d2 in matched_diagnoses:
					if (d1 != d2) & (fuzz.ratio(d1, d2) / 100 >= 0.98):
						matched_diagnoses.remove(d2)

			spark_df = spark_df.where(spark_df.diagnosis.isin(matched_diagnoses))
			print(f"\nDiagnoses related to '{selected_diagnosis}':\n")
			lookup_tbl = spark_df.toPandas()
			print(lookup_tbl)

		else:
			print(f"\nDiagnoses related to '{selected_diagnosis}':\n")
			if flag:
				lookup_tbl =  pd.DataFrame(get_children(id=str(selected_id), source='athena'))
			else:
				lookup_tbl = pd.DataFrame(get_children(id=str(selected_id), source='icd-11'))

			if len(lookup_tbl) > 0:
				print(lookup_tbl)
			else:
				print(f"Diagnosis '{selected_diagnosis}' has been selected because no immediate suggestions were found...")
			decision.append(selected_diagnosis)

		while True:
			keep_searching = input("Continue searching? (y/n): ")
			if keep_searching == 'n':
				print(f"Diagnosis '{selected_diagnosis}' has been selected.")
				print("\nExiting.")
				break_flag = True
				break
			elif keep_searching == 'y':
				print("\nKeep searching...")
				decision.append(selected_diagnosis)
				break
			else:
				print("\nInvalid input. Please type again your answer.")
		
		if break_flag:
			break
		cnt += 1